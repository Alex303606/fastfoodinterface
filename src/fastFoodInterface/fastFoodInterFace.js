import React, {Component} from 'react';
import './fastFoodInterFace.css';
import Items from "./components/items/items";
import OrderDetails from "./components/orderDetails/OrderDetails";
import images from './fastFoodImages'

class FastFoodInterface extends Component {
	state = {
		items: [
			{name: 'Hamburger', price: 80, amount: 0, image: images.burger},
			{name: 'Cola', price: 30, amount: 0, image: images.cola},
			{name: 'Coffee', price: 20, amount: 0, image: images.coffee},
			{name: 'Tea', price: 15, amount: 0, image: images.tea},
			{name: 'Fries', price: 45, amount: 0, image: images.fries },
			{name: 'Cheeseburger', price: 100, amount: 0, image: images.cheeseburger},
			{name: 'Samsa', price: 50, amount: 0, image: images.samsa},
			{name: 'Hotdog', price: 60, amount: 0, image: images.hotdog},
			{name: 'Shaurma', price: 120, amount: 0, image: images.shaurma},
			{name: 'Beer', price: 75, amount: 0, image: images.beer},
			{name: 'Pizza', price: 350, amount: 0, image: images.pizza},
		],
		totalPrice: 0
	};
	
	addItem = (name) => {
		const items = [...this.state.items];
		const index = items.findIndex(p => p.name === name);
		items[index].amount++;
		let totalPrice = this.state.totalPrice;
		totalPrice += items[index].price;
		this.setState({items, totalPrice});
	};
	
	removeItem = (name) => {
		const items = [...this.state.items];
		const index = items.findIndex(p => p.name === name);
		let totalPrice = this.state.totalPrice;
		totalPrice -= items[index].price * items[index].amount;
		items[index].amount = 0;
		this.setState({items,totalPrice});
	};
	
	render() {
		return (
			<div className="FastFoodInterface">
				<OrderDetails removeItem={this.removeItem} totalPrice={this.state.totalPrice} order={this.state.items}/>
				<Items addItem={this.addItem} items={this.state.items}/>
			</div>
		);
	}
}

export default FastFoodInterface;