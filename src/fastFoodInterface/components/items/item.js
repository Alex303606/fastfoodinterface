import React from 'react';

const Item = props => {
	return (
		<div className="AddItems__items--item" onClick={() => props.add(props.thisItem.name)}>
			<div className="AddItems__items--item--icon">
				<img src={props.thisItem.image} alt=""/>
			</div>
			<div className="AddItems__items--item--descr">
				<div className="AddItems__items--item--name">{props.thisItem.name}</div>
				<div className="AddItems__items--item--price">Price: {props.thisItem.price} KGS</div>
			</div>
		</div>
	)
};

export default Item;