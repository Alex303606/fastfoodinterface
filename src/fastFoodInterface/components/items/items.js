import React from 'react';
import Item from "./item";

const Items = props => {
	return (
		<div className="AddItems">
			<div className="AddItems__title">Add items</div>
			<div className="AddItems__items">
				{
					props.items.map(item => {
						return <Item
							thisItem={item}
							key={item.name + item.amount}
							add={props.addItem}
						/>
					})
				}
			</div>
		</div>
	)
};

export default Items;