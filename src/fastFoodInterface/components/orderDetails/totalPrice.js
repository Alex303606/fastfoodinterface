import React from 'react';

const TotalPrice = props => {
	return (
		<div className="TotalPrice"><span className="TotalPrice__title">Total price:</span> {props.total} KGS</div>
	)
};

export default TotalPrice;