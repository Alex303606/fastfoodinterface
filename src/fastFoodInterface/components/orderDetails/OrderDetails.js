import React from 'react';
import OrderItem from "./orderItem";
import TotalPrice from "./totalPrice";

const OrderDetails = props => {
	let text = null;
	let total = <TotalPrice total={props.totalPrice}/>;
	
	if(props.order.every(item => item.amount === 0)) {
		text = 'Order is empty! Add some items!';
		total = null;
	}
	return (
		<div className="OrderDetails">
			<div className="OrderDetails__title">Order Details</div>
			<div className="OrderDetails__inner">
				{text}
				{
					props.order.map(orderItem => {
						if(orderItem.amount > 0){
							return <OrderItem
								item={orderItem}
								key={orderItem.name + orderItem.amount}
								remove={props.removeItem}
							/>;
						} else {return null;}
					})
				}
				{total}
			</div>
		</div>
	)
};

export default OrderDetails;