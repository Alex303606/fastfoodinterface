import React from 'react';

const OrderItem = props => {
	return (
		<div className="OrderItem">
			<img src={props.item.image} alt=""/>
			<span className="OrderItem__name">{props.item.name} </span>
			<span className="OrderItem__amount">x {props.item.amount} </span>
			<span className="OrderItem__price">{props.item.price} KGS</span>
			<button className="OrderItem__delete" onClick={() => props.remove(props.item.name)}>
				<i className="fa fa-times" aria-hidden="true"></i>
			</button>
		</div>
	)
};

export default OrderItem;